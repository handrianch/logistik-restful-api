package main

import (
	"log"
	"net/http"

	"github.com/handrianch/RestAPILogistik/database"
	"github.com/handrianch/RestAPILogistik/middleware"
	"github.com/handrianch/RestAPILogistik/routes"
)

func main() {
	router := routes.Router
	database.InitialMigration()
	log.Fatal(http.ListenAndServe(":3000", middleware.RemoveTrailingSlash(router)))
}
