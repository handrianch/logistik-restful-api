package routes

import (
	"github.com/gorilla/mux"
)

// Router Base
var Router = mux.NewRouter().PathPrefix("/api").Subrouter()
