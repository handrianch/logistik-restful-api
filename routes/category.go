package routes

import (
	"github.com/handrianch/RestAPILogistik/controllers"
)

var category = controllers.CategoryController{}

func init() {
	Router.HandleFunc("/categories", category.Index).Methods("GET")
	Router.HandleFunc("/categories/{id}", category.Show).Methods("GET")
	Router.HandleFunc("/categories", category.Store).Methods("POST")
	Router.HandleFunc("/categories/{id}", category.Update).Methods("PUT")
	Router.HandleFunc("/categories/{id}", category.Destroy).Methods("DELETE")
}
