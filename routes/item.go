package routes

import (
	"github.com/handrianch/RestAPILogistik/controllers"
)

var item = controllers.ItemController{}

func init() {
	Router.HandleFunc("/items", item.Index).Methods("GET")
	Router.HandleFunc("/items/{id}", item.Show).Methods("GET")
	Router.HandleFunc("/items", item.Store).Methods("POST")
	Router.HandleFunc("/items/{id}", item.Update).Methods("PUT")
	Router.HandleFunc("/items/{id}", item.Destroy).Methods("DELETE")
}
