package models

// Item Model
type Item struct {
	ID         uint      `gorm:"PRIMARY_KEY;AUTO_INCREMENT" json:"id"`
	ItemID     string    `gorm:"type:varchar(4);UNIQUE;NOT NULL" json:"item_id"` // ItemID
	CategoryID uint      `json:"category_id"`
	Category   *Category `gorm:"foreignkey:category_id" json:"category,omitempty"`
}
