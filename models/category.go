package models

// Category Model
type Category struct {
	ID       uint   `gorm:"PRIMARY_KEY;AUTO_INCREMENT" json:"id"`
	Category string `gorm:"NOT NULL" json:"category"`
}
