package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/handrianch/RestAPILogistik/models"
)

// CategoryController Object
type CategoryController struct{}

// Index for get all data
func (c CategoryController) Index(w http.ResponseWriter, r *http.Request) {
	var categories []models.Category
	db.Find(&categories)

	json.NewEncoder(w).Encode(categories)
}

// Show single data
func (c CategoryController) Show(w http.ResponseWriter, r *http.Request) {
	var category models.Category

	params := mux.Vars(r)
	categoryID, err := strconv.Atoi(params["id"])

	if err != nil {
		panic(err)
	}

	notFound := db.First(&category, categoryID).RecordNotFound()

	if notFound {
		json.NewEncoder(w).Encode([]struct{}{})
	} else {
		json.NewEncoder(w).Encode(category)
	}

}

// Store data
func (c CategoryController) Store(w http.ResponseWriter, r *http.Request) {
	var category models.Category

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&category)

	if err != nil {
		panic(err)
	}

	db.Create(&category)
	json.NewEncoder(w).Encode(category)
}

// Update data
func (c CategoryController) Update(w http.ResponseWriter, r *http.Request) {
	var category, oldCategory models.Category

	defer r.Body.Close()
	json.NewDecoder(r.Body).Decode(&oldCategory)

	params := mux.Vars(r)
	categoryID, err := strconv.Atoi(params["id"])

	if err != nil {
		panic(err)
	}

	notFound := db.First(&category, categoryID).RecordNotFound()

	if notFound {
		panic("not found")
	}

	db.Model(&category).Updates(oldCategory)

	json.NewEncoder(w).Encode(category)
}

// Destroy data
func (c CategoryController) Destroy(w http.ResponseWriter, r *http.Request) {
	var category models.Category

	params := mux.Vars(r)
	categoryID, err := strconv.Atoi(params["id"])

	if err != nil {
		panic(err)
	}

	notFound := db.First(&category, categoryID).RecordNotFound()

	if notFound {
		http.Error(w, "Not Found", http.StatusNotFound)
	} else {
		db.Delete(&category)

		json.NewEncoder(w).Encode([]struct{}{})
	}
}
