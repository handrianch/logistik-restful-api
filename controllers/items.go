package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/handrianch/RestAPILogistik/models"
)

// ItemController Object
type ItemController struct{}

// Index for get all data
func (i ItemController) Index(w http.ResponseWriter, r *http.Request) {
	var Items []models.Item

	db.Preload("Category").Find(&Items)
	json.NewEncoder(w).Encode(Items)
}

// Show single data
func (i ItemController) Show(w http.ResponseWriter, r *http.Request) {
	var Item models.Item

	params := mux.Vars(r)
	itemID, err := strconv.Atoi(params["id"])

	if err != nil {
		panic(err)
	}

	db.Preload("Category").First(&Item, itemID)

	if (models.Item{} == Item) {
		json.NewEncoder(w).Encode([]struct{}{})
	} else {
		json.NewEncoder(w).Encode(Item)
	}
}

// Store data
func (i ItemController) Store(w http.ResponseWriter, r *http.Request) {
	var Item models.Item

	defer r.Body.Close()

	err := json.NewDecoder(r.Body).Decode(&Item)

	if err != nil {
		panic(err)
	}

	db.Create(&Item)

	json.NewEncoder(w).Encode(&Item)
}

// Update data
func (i ItemController) Update(w http.ResponseWriter, r *http.Request) {
	var NewItem, OldItem models.Item

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&NewItem)

	if err != nil {
		panic(err)
	}

	params := mux.Vars(r)
	itemID, err := strconv.Atoi(params["id"])

	if err != nil {
		panic(err)
	}

	notFound := db.First(&OldItem, itemID).RecordNotFound()

	if notFound {
		http.Error(w, "Not Found", http.StatusNotFound)
	} else {
		db.Model(&OldItem).Updates(&NewItem)

		json.NewEncoder(w).Encode(&NewItem)
	}
}

// Destroy data
func (i ItemController) Destroy(w http.ResponseWriter, r *http.Request) {
	var Item models.Item

	params := mux.Vars(r)
	itemID, err := strconv.Atoi(params["id"])

	if err != nil {
		panic(err)
	}

	notFound := db.First(&Item, itemID).RecordNotFound()

	if notFound {
		http.Error(w, "Not Found", http.StatusNotFound)
	} else {
		db.Delete(&Item)

		json.NewEncoder(w).Encode([]struct{}{})
	}
}
