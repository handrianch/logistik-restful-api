package controllers

import "github.com/handrianch/RestAPILogistik/database"

var db = database.GetConnection()

func main() {
	defer db.Close()
}
