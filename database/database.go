package database

import (
	"github.com/handrianch/RestAPILogistik/models"
	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/sqlite" // driver for gorm
)

// GetConnection database
func GetConnection() *gorm.DB {
	db, err := gorm.Open("sqlite3", "./database/logistik.db")

	if err != nil {
		panic(err)
	}

	return db
}

// InitialMigration for create db
func InitialMigration() {
	db := GetConnection()
	defer db.Close()

	db.AutoMigrate(&models.Category{}, models.Item{})
}
